use crate::binary::Binary;

pub fn compute_hamming_distance(a: &Binary, b: &Binary) -> usize {
    a.chars().zip(b.chars()).filter(|(a, b)| a != b).count()
}

pub fn break_xor(binary: Binary) -> usize {
    // let key_size = 2..=40;
    // let mut t = key_size
    //     .into_iter()
    //     .map(|key_size| {
    //         let a = &binary[..key_size * 2];
    //         let b = &binary[key_size * 2..key_size * 2 * 2];
    //         (key_size, compute_hamming_distance(a, b) / key_size)
    //     })
    //     .collect::<Vec<(usize, usize)>>();

    // t.sort_by(|a, b| a.1.cmp(&b.1));

    // let smallest_3: Vec<usize> = t[..3].into_iter().map(|x| x.0).collect();

    // println!("{:?}", smallest_3);

    0
}

#[cfg(test)]
mod tests {
    use std::fs;

    use crate::{base64::Base64, binary::ToBinary};

    use super::*;

    #[test]
    fn break_xor_test() {
        let contents: Base64 = fs::read_to_string("./files/6.txt")
            .expect("Should have been able to read the file")
            .parse()
            .unwrap();

        let binary = contents.to_binary();

        let result = break_xor(binary);
        assert_eq!(result, 1);
    }

    #[test]
    fn hamming_distance_test() {
        let a: Binary = "this is a test".to_owned().to_binary();
        let b: Binary = "wokka wokka!!!".to_owned().to_binary();

        let result = compute_hamming_distance(&a, &b);

        assert_eq!(result, 37);
    }
}
