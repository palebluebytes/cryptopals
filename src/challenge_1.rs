use crate::{base64::ToBase64, binary::ToBinary, hex::Hex};

// hex is 0 - 9 and A - F, each hex code refers to 4 binary
pub fn hex_to_base64_naive(hex: &str) -> String {
    let mut binary: String = hex
        .chars()
        .map(|code| match code {
            '0' => "0000",
            '1' => "0001",
            '2' => "0010",
            '3' => "0011",
            '4' => "0100",
            '5' => "0101",
            '6' => "0110",
            '7' => "0111",
            '8' => "1000",
            '9' => "1001",
            'a' => "1010",
            'b' => "1011",
            'c' => "1100",
            'd' => "1101",
            'e' => "1110",
            'f' => "1111",
            _ => panic!("Not valid hex code!"),
        })
        .collect();

    while binary.len() % 6 != 0 {
        binary.push('0');
    }

    let mut base64: String = binary
        .as_bytes()
        .chunks(6)
        .map(|bits| match std::str::from_utf8(bits).unwrap() {
            "000000" => "A",
            "000001" => "B",
            "000010" => "C",
            "000011" => "D",
            "000100" => "E",
            "000101" => "F",
            "000110" => "G",
            "000111" => "H",
            "001000" => "I",
            "001001" => "J",
            "001010" => "K",
            "001011" => "L",
            "001100" => "M",
            "001101" => "N",
            "001110" => "O",
            "001111" => "P",
            "010000" => "Q",
            "010001" => "R",
            "010010" => "S",
            "010011" => "T",
            "010100" => "U",
            "010101" => "V",
            "010110" => "W",
            "010111" => "X",
            "011000" => "Y",
            "011001" => "Z",
            "011010" => "a",
            "011011" => "b",
            "011100" => "c",
            "011101" => "d",
            "011110" => "e",
            "011111" => "f",
            "100000" => "g",
            "100001" => "h",
            "100010" => "i",
            "100011" => "j",
            "100100" => "k",
            "100101" => "l",
            "100110" => "m",
            "100111" => "n",
            "101000" => "o",
            "101001" => "p",
            "101010" => "q",
            "101011" => "r",
            "101100" => "s",
            "101101" => "t",
            "101110" => "u",
            "101111" => "v",
            "110000" => "w",
            "110001" => "x",
            "110010" => "y",
            "110011" => "z",
            "110100" => "0",
            "110101" => "1",
            "110110" => "2",
            "110111" => "3",
            "111000" => "4",
            "111001" => "5",
            "111010" => "6",
            "111011" => "7",
            "111100" => "8",
            "111101" => "9",
            "111110" => "+",
            "111111" => "/",
            _ => panic!("Incorrect bit chunking for base64!"),
        })
        .collect();

    while base64.len() % 4 != 0 {
        base64.push('=');
    }

    base64
}

pub fn hex_to_base64(hex: &str) -> String {
    let hex: Hex = hex.to_owned().parse().unwrap();
    let binary = hex.to_binary();
    let base64 = binary.to_base64();

    base64.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    const HEX: &str = "68656c6c6f20776f726c64";
    const BASE64: &str = "aGVsbG8gd29ybGQ=";

    #[test]
    fn hex_to_base64_naive_test() {
        let result = hex_to_base64_naive(HEX);
        assert_eq!(result, BASE64);
    }

    #[test]
    fn hex_to_base64_test() {
        assert_eq!(hex_to_base64(HEX), BASE64);
    }
}
