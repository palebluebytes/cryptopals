pub trait ToAscii {
    fn to_ascii(&self) -> String;
}
