pub const HEX_CHARS: &str = "0123456789abcdef";
pub const BASE64_CHARS: &str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

pub fn hex_to_binary(hex: &str) -> Result<String, &'static str> {
    hex.to_ascii_lowercase()
        .chars()
        .map(|c| match c {
            '0'..='9' | 'a'..='f' => Ok(format!("{:04b}", c.to_digit(16).unwrap())),
            _ => Err("Invalid hex character"),
        })
        .collect()
}

pub fn hex_to_string(hex: &str) -> String {
    hex.as_bytes()
        .chunks(2)
        .map(|chunk| {
            let hex_pair = std::str::from_utf8(chunk).unwrap();
            let code_point = u8::from_str_radix(hex_pair, 16).unwrap();
            code_point as char
        })
        .collect()
}

pub fn base64_to_binary(base64: &str) -> String {
    let binary: String = base64
        .chars()
        .filter(|x| x != &'=')
        .map(|c| {
            let binary = BASE64_CHARS.chars().position(|x| c == x).unwrap();
            // This format pads the binary to be the correct 6 bits that base64 represents
            let binary = format!("{binary:06b}");
            binary
        })
        .collect();

    // Binary is made up of 8 bits aka bytes
    // Base64 is made up of 6 bits
    // This means the final number of bits in the above binary can be wrong
    // To fix this we need to align the collected bits to fit within the
    // 8 bit aka byte length
    let extra_bits = binary.len() % 8;

    binary[0..binary.len() - extra_bits].to_owned()
}

pub fn binary_to_hex(binary: &str) -> Result<String, &'static str> {
    let hex: String = binary
        .as_bytes()
        .chunks(4)
        .map(|chunk| {
            let bits = std::str::from_utf8(chunk).unwrap();
            let value = usize::from_str_radix(bits, 2).unwrap();
            HEX_CHARS.chars().nth(value).unwrap()
        })
        .collect();

    Ok(hex)
}

pub fn binary_to_base64(binary: &str) -> Result<String, &'static str> {
    let mut base64: String = binary
        .as_bytes()
        .chunks(6)
        .map(|chunk| {
            let bits = std::str::from_utf8(chunk).unwrap();
            // The final byte may need to be padded to to 6 bits
            // This padding happens at the front in order not to change the
            // numeric value represented by the bits
            let bits = format!("{bits:0<6}");
            let value = usize::from_str_radix(&bits, 2).unwrap();
            BASE64_CHARS.chars().nth(value).unwrap()
        })
        .collect();

    while base64.len() % 4 != 0 {
        base64.push('=');
    }

    Ok(base64)
}

pub fn string_to_hex(string: &str) -> String {
    string
        .chars()
        .map(|c| {
            let code_point = u32::from(c);
            format!("{:02x}", code_point)
        })
        .collect::<String>()
        .to_lowercase()
}

pub fn string_to_binary(string: &str) -> String {
    string
        .chars()
        .map(|c| {
            let code_point = u32::from(c);
            format!("{code_point:08b}")
        })
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    const HEX: &str = "68656c6c6f20776f726c64";
    const BASE64: &str = "aGVsbG8gd29ybGQ=";
    const BINARY: &str =
        "0110100001100101011011000110110001101111001000000111011101101111011100100110110001100100";
    const STRING: &str = "hello world";

    #[test]
    fn hex_to_binary_test() {
        assert_eq!(hex_to_binary(HEX).unwrap(), BINARY);
    }

    #[test]
    fn hex_to_string_test() {
        assert_eq!(hex_to_string(HEX), STRING);
    }

    #[test]
    fn binary_to_hex_test() {
        assert_eq!(binary_to_hex(BINARY).unwrap(), HEX);
    }

    #[test]
    fn binary_to_base64_test() {
        assert_eq!(binary_to_base64(BINARY).unwrap(), BASE64);
    }

    #[test]
    fn base64_to_binary_test() {
        let result = base64_to_binary(BASE64);

        assert_eq!(result, BINARY);
    }

    #[test]
    fn string_to_hex_test() {
        assert_eq!(string_to_hex(STRING), HEX)
    }

    #[test]
    fn string_to_binary_test() {
        assert_eq!(string_to_binary(STRING), BINARY);
    }
}
