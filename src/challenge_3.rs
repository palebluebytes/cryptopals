use crate::{ascii::ToAscii, binary::ToBinary, challenge_2::xor, hex::Hex};

// TODO
// You can judge the frequency of character repetitions in the encrypted message
// and use that to narrow down the list of possible text that needs to be
// unencrypted
//fn frequency_analysis(message: &str) -> Vec<String> {

//}

// A very naive approach
fn score_message(message: &str) -> u32 {
    message.chars().fold(0, |acc, char| {
        if char == ' ' {
            return acc + 1;
        } else if char.is_ascii_alphabetic() {
            return acc + 1;
        } else {
            return acc;
        }
    })
}

pub fn single_byte_xor_cipher(encrypted_message: Hex) -> Result<(String, u32, char), &'static str> {
    let cipher_length = encrypted_message.len() / 2;

    (0..=255u8)
        .into_iter()
        .map(|key| {
            let hex_key = format!("{:02x}", key);

            let cipher = hex_key
                .repeat(cipher_length)
                .parse::<Hex>()
                .unwrap()
                .to_binary();

            let encrypted_message = encrypted_message.to_binary();

            let message = xor(&encrypted_message, &cipher).unwrap().to_ascii();

            let score = score_message(&message);

            let key = key as char;

            (message, score, key)
        })
        .max_by_key(|&(_, score, _)| score)
        .ok_or("No valid decryption found")
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn cipher_test() {
        let hex: Hex = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"
            .parse()
            .unwrap();

        let result = single_byte_xor_cipher(hex);

        println!("{:?}", result);

        assert_eq!(result.unwrap().0, "Cooking MC's like a pound of bacon");
    }
}
