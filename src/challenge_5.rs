use crate::{
    binary::{Binary, ToBinary},
    challenge_2::xor,
    hex::{Hex, ToHex},
};

pub fn repeating_key_xor_encryption(message: &str, key: &str) -> Hex {
    let cipher: Binary = (0..message.len())
        .into_iter()
        .map(|index| key.as_bytes()[index % key.len()] as char)
        .collect::<String>()
        .to_binary();

    let encrypted_message: Binary = message.to_owned().to_binary();

    xor(&encrypted_message, &cipher).unwrap().to_hex()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn repeating_key_xor_encryption_test() {
        let hex: Hex = "0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f".parse().unwrap();

        let result = repeating_key_xor_encryption(
            "Burning 'em, if you ain't quick and nimble
I go crazy when I hear a cymbal",
            "ICE",
        );

        assert_eq!(result, hex)
    }
}
