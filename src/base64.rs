use std::fmt;
use std::ops::Deref;
use std::str::FromStr;

use crate::binary::{Binary, ToBinary};

pub const BASE64_CHARS: &str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Base64(String);

pub trait ToBase64 {
    fn to_base64(&self) -> Base64;
}

#[derive(Debug, PartialEq)]
pub enum Base64ParseError {
    InvalidFormat,
}
impl fmt::Display for Base64ParseError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self)
    }
}
impl std::error::Error for Base64ParseError {}

impl FromStr for Base64 {
    type Err = Base64ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let check = s.chars().try_for_each(|c| {
            let valid_characters = BASE64_CHARS.to_string() + "=";
            if None == valid_characters.find(c) {
                return Err(c);
            };

            Ok(())
        });

        match check {
            Result::Ok(_) => Ok(s.parse().unwrap()),
            // How can I pass the character to the Error?
            Result::Err(_c) => Err(Base64ParseError::InvalidFormat),
        }
    }
}

impl fmt::Display for Base64 {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl Deref for Base64 {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl ToBinary for Base64 {
    fn to_binary(&self) -> Binary {
        let binary: String = self
            .0
            .chars()
            .filter(|x| x != &'=')
            .map(|c| {
                let binary = BASE64_CHARS.chars().position(|x| c == x).unwrap();
                // This format pads the binary to be the correct 6 bits that base64 represents
                let binary = format!("{binary:06b}");
                binary
            })
            .collect();

        // Binary is made up of 8 bits aka bytes
        // Base64 is made up of 6 bits
        // This means the final number of bits in the above binary can be wrong
        // To fix this we need to align the collected bits to fit within the
        // 8 bit aka byte length
        let extra_bits = binary.len() % 8;

        let binary = binary[0..binary.len() - extra_bits].to_owned();

        binary.parse().unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const BASE64: &str = "aGVsbG8gd29ybGQ=";
    const BINARY: &str =
        "0110100001100101011011000110110001101111001000000111011101101111011100100110110001100100";

    #[test]
    fn base64_to_binary() {
        let base64: Base64 = BASE64.parse().unwrap();
        let binary = base64.to_binary().to_string();

        assert_eq!(binary, BINARY);
    }
}
