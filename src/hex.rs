use std::fmt;
use std::ops::Deref;
use std::str::FromStr;

use crate::{
    ascii::ToAscii,
    binary::{Binary, ToBinary},
};

pub const HEX_CHARS: &str = "0123456789abcdef";

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Hex(String);

pub trait ToHex {
    fn to_hex(&self) -> Hex;
}

#[derive(Debug, PartialEq)]
pub enum HexParseError {
    InvalidFormat,
}
impl fmt::Display for HexParseError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self)
    }
}
impl std::error::Error for HexParseError {}

impl FromStr for Hex {
    type Err = HexParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let s = s.to_ascii_lowercase();
        let check = s.chars().try_for_each(|c| {
            if None == HEX_CHARS.find(c) {
                return Err(c);
            }

            Ok(())
        });

        match check {
            Result::Ok(_) => Ok(Hex(s)),
            // How can I pass the character to the Error?
            Result::Err(_c) => Err(HexParseError::InvalidFormat),
        }
    }
}

impl ToAscii for Hex {
    fn to_ascii(&self) -> String {
        self.0
            .as_bytes()
            .chunks(2)
            .map(|chunk| {
                let hex_pair = std::str::from_utf8(chunk).unwrap();
                let code_point = u8::from_str_radix(hex_pair, 16).unwrap();
                code_point as char
            })
            .collect()
    }
}

impl fmt::Display for Hex {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl Deref for Hex {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl ToBinary for Hex {
    fn to_binary(&self) -> Binary {
        let binary: String = self
            .0
            .chars()
            .map(|c| format!("{:04b}", c.to_digit(16).unwrap()))
            .collect();

        binary.parse().unwrap()
    }
}

impl ToHex for String {
    fn to_hex(&self) -> Hex {
        let hex = self
            .chars()
            .map(|c| {
                let code_point = u32::from(c);
                format!("{:02x}", code_point)
            })
            .collect::<String>()
            .to_lowercase();

        hex.parse().unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const HEX: &str = "68656c6c6f20776f726c64";
    const BINARY: &str =
        "0110100001100101011011000110110001101111001000000111011101101111011100100110110001100100";
    const STRING: &str = "hello world";

    #[test]
    fn hex_to_binary() {
        let hex: Hex = HEX.parse().unwrap();
        let binary = hex.to_binary().to_string();

        assert_eq!(binary, BINARY);
    }

    #[test]
    fn string_to_hex() {
        let hex = STRING.to_owned().to_hex().to_string();
        assert_eq!(hex, HEX)
    }

    #[test]
    fn hex_to_ascii() {
        let hex: Hex = HEX.parse().unwrap();
        let string = hex.to_ascii();

        assert_eq!(string, STRING);
    }
}
