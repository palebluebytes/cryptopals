use std::path::Path;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};

use crate::challenge_3::single_byte_xor_cipher;
use crate::hex::Hex;

pub fn cipher_file<P>(filename: P) -> Result<(String, u32, char), &'static str>
where
    P: AsRef<Path>,
{
    let file = File::open(filename).map_err(|_| "Failed to open file")?;

    let lines = BufReader::new(file).lines();

    lines
        .filter_map(Result::ok)
        .filter_map(|line| single_byte_xor_cipher(line.parse::<Hex>().unwrap()).ok())
        .max_by_key(|&(_, score, _)| score)
        .ok_or("No valid decryption found")
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn cipher_file_test() {
        let result = cipher_file("./files/4.txt").unwrap();

        println!("key is {}", result.2);

        assert_eq!(result.0, "Now that the party is jumping\n");
    }
}
