use crate::binary::Binary;

pub fn xor(first: &Binary, second: &Binary) -> Result<Binary, &'static str> {
    if first.len() != second.len() {
        return Err("Input strings must be of equal length");
    }

    let xor: String = first
        .chars()
        .zip(second.chars())
        .map(|(bit1, bit2)| {
            match (bit1, bit2) {
                ('0', '0') | ('1', '1') => '0',
                ('0', '1') | ('1', '0') => '1',
                _ => return '0', // Default case
            }
        })
        .collect();

    Ok(xor.parse().unwrap())
}

#[cfg(test)]
mod tests {
    use crate::{
        binary::ToBinary,
        hex::{Hex, ToHex},
    };

    use super::*;

    #[test]
    fn xor_test() {
        let hex: Hex = "746865206b696420646f6e277420706c6179".parse().unwrap();

        let first = "1c0111001f010100061a024b53535009181c"
            .parse::<Hex>()
            .unwrap()
            .to_binary();

        let second = "686974207468652062756c6c277320657965"
            .parse::<Hex>()
            .unwrap()
            .to_binary();

        let result = xor(&first, &second).unwrap().to_hex();

        assert_eq!(result, hex)
    }
}
