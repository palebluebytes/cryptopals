use std::fmt;
use std::ops::Deref;
use std::str::FromStr;

use crate::{
    ascii::ToAscii,
    base64::{Base64, ToBase64, BASE64_CHARS},
    hex::{Hex, ToHex, HEX_CHARS},
};

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Binary(String);

pub trait ToBinary {
    fn to_binary(&self) -> Binary;
}

impl FromStr for Binary {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Binary(s.to_string()))
    }
}

impl fmt::Display for Binary {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl Deref for Binary {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl ToHex for Binary {
    fn to_hex(&self) -> Hex {
        let hex: String = self
            .0
            .as_bytes()
            .chunks(4)
            .map(|chunk| {
                let bits = std::str::from_utf8(chunk).unwrap();
                let value = usize::from_str_radix(bits, 2).unwrap();
                HEX_CHARS.chars().nth(value).unwrap()
            })
            .collect();

        hex.parse().unwrap()
    }
}

impl ToBase64 for Binary {
    fn to_base64(&self) -> Base64 {
        let mut base64: String = self
            .0
            .as_bytes()
            .chunks(6)
            .map(|chunk| {
                let bits = std::str::from_utf8(chunk).unwrap();
                // The final byte may need to be padded to to 6 bits
                // This padding happens at the front in order not to change the
                // numeric value represented by the bits
                let bits = format!("{bits:0<6}");
                let value = usize::from_str_radix(&bits, 2).unwrap();
                BASE64_CHARS.chars().nth(value).unwrap()
            })
            .collect();

        while base64.len() % 4 != 0 {
            base64.push('=');
        }

        base64.parse().unwrap()
    }
}

impl ToBinary for String {
    fn to_binary(&self) -> Binary {
        let binary: String = self
            .chars()
            .map(|c| {
                let code_point = u32::from(c);
                format!("{code_point:08b}")
            })
            .collect();

        binary.parse().unwrap()
    }
}

impl ToAscii for Binary {
    fn to_ascii(&self) -> String {
        self.0
            .as_bytes()
            .chunks(8)
            .map(|chunk| {
                let chunk = std::str::from_utf8(chunk).unwrap();
                let code_point = u8::from_str_radix(chunk, 2).unwrap();
                code_point as char
            })
            .collect()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const HEX: &str = "68656c6c6f20776f726c64";
    const BASE64: &str = "aGVsbG8gd29ybGQ=";
    const BINARY: &str =
        "0110100001100101011011000110110001101111001000000111011101101111011100100110110001100100";
    const STRING: &str = "hello world";

    #[test]
    fn binary_to_hex() {
        let binary: Binary = BINARY.parse().unwrap();
        let hex = binary.to_hex().to_string();

        assert_eq!(hex, HEX);
    }

    #[test]
    fn binary_to_base64() {
        let binary: Binary = BINARY.parse().unwrap();
        let base64 = binary.to_base64().to_string();
        assert_eq!(base64, BASE64);
    }

    #[test]
    fn string_to_binary() {
        let binary = STRING.to_owned().to_binary().to_string();
        assert_eq!(binary, BINARY);
    }

    #[test]
    fn binary_to_ascii() {
        let binary: Binary = BINARY.parse().unwrap();
        let string = binary.to_ascii();

        assert_eq!(string, STRING);
    }
}
